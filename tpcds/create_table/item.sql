CREATE TABLE  if not exists `item` (
i_item_sk                 int,
      i_item_id                 string,
      i_rec_start_date          date,
      i_rec_end_date            date,
      i_item_desc               string,
      i_current_price           decimal(7,2),
      i_wholesale_cost          decimal(7,2),
      i_brand_id                int,
      i_brand                   string,
      i_class_id                int,
      i_class                   string,
      i_category_id             int,
      i_category                string,
      i_manufact_id             int,
      i_manufact                string,
      i_size                    string,
      i_formulation             string,
      i_color                   string,
      i_units                   string,
      i_container               string,
      i_manager_id              int,
      i_product_name            string)
USING parquet
    OPTIONS (
  path '${spark.tpcds.parquet.data.folder}/item'
)