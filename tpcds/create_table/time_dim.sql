CREATE TABLE  if not exists `time_dim` (
t_time_sk                 int,
      t_time_id                 string,
      t_time                    int,
      t_hour                    int,
      t_minute                  int,
      t_second                  int,
      t_am_pm                   string,
      t_shift                   string,
      t_sub_shift               string,
      t_meal_time               string)
      USING parquet
    OPTIONS (
  path '${spark.tpcds.parquet.data.folder}/time_dim'
)