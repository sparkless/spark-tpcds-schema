CREATE TABLE if not exists  `income_band` (
          ib_income_band_sk         int
    ,     ib_lower_bound            int
    ,     ib_upper_bound            int
    )
USING parquet
    OPTIONS (
  path '${spark.tpcds.parquet.data.folder}/income_band'
)