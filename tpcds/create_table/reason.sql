CREATE TABLE  if not exists `reason`(
r_reason_sk               int
    ,     r_reason_id               string
    ,     r_reason_desc             string)
USING parquet
    OPTIONS (
  path '${spark.tpcds.parquet.data.folder}/reason'
)