CREATE TABLE if not exists  `inventory` (
      inv_item_sk int,
      inv_warehouse_sk int,
      inv_quantity_on_hand int,
      inv_date_sk int)
USING parquet
    OPTIONS (
  path '${spark.tpcds.parquet.data.folder}/inventory'
)
partitioned by (inv_date_sk)