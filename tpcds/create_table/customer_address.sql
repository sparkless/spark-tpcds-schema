CREATE TABLE if not exists  `customer_address` (
      ca_address_sk             int,
      ca_address_id             string,
      ca_street_number          string,
      ca_street_name            string,
      ca_street_type            string,
      ca_suite_number           string,
      ca_city                   string,
      ca_county                 string,
      ca_state                  string,
      ca_zip                    string,
      ca_country                string,
      ca_gmt_offset             decimal(5,2),
      ca_data_folder_type          string
      )
USING parquet
    OPTIONS (
  path '${spark.tpcds.parquet.data.folder}/customer_address'
)