--
-- History
--    hb  08/21   copied from existing design
--    hb  08/21   change large keys to metric: addr_sk, cdemo_sk,
--                   cust_sk, hdemo_sk, item_sk, date_sk, time_sk
--    hb  08/21   remove avgsizeperpartition option

create olap index catalog_sales_snap on catalog_sales
dimension c_birth_month is nullable nullvalue "-93"
metric c_birth_year aggregator doubleSum is nullable nullvalue "-93"
metric c_current_addr_sk aggregator longSum
metric c_current_cdemo_sk aggregator longSum
metric c_current_hdemo_sk aggregator longSum
dimension c_first_name is nullable nullvalue "-93"
metric c_first_sales_date_sk aggregator longSum is nullable nullvalue "-1"
metric c_first_shipto_date_sk aggregator longSum is nullable nullvalue "-1"
dimension c_last_name is nullable nullvalue "-93"
dimension c_preferred_cust_flag is nullable nullvalue "-93"
dimension ca_city is nullable nullvalue "-93"
dimension ca_country is nullable nullvalue "-93"
dimension ca_county is nullable nullvalue "-93"
dimension ca_gmt_offset is nullable nullvalue "-999.91"
dimension ca_state is nullable nullvalue "-93"
dimension ca_zip is nullable nullvalue "-93"
dimension cc_call_center_id is nullable nullvalue "-93"
dimension cc_county is nullable nullvalue "-93"
dimension cc_manager is nullable nullvalue "-93"
dimension cc_name is nullable nullvalue "-93"
dimension cd_credit_rating is nullable nullvalue "-93"
dimension cd_dep_college_count is nullable nullvalue "-93"
dimension cd_dep_count is nullable nullvalue "-93"
dimension cd_dep_employed_count is nullable nullvalue "-93"
dimension cd_education_status is nullable nullvalue "-93"
dimension cd_gender is nullable nullvalue "-93"
dimension cd_marital_status is nullable nullvalue "-93"
dimension cd_purchase_estimate is nullable nullvalue "-93"
metric cp_catalog_page_id aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_bill_addr_sk aggregator longSum
metric cs_bill_cdemo_sk aggregator longSum
metric cs_bill_customer_sk aggregator longSum
metric cs_bill_hdemo_sk aggregator longSum
dimension cs_call_center_sk is nullable nullvalue "-93"
dimension cs_catalog_page_sk is nullable nullvalue "-93"
metric cs_coupon_amt aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_ext_discount_amt aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_ext_list_price aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_ext_sales_price aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_ext_ship_cost aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_ext_tax aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_ext_wholesale_cost aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_item_sk aggregator longSum
metric cs_list_price aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_net_paid aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_net_paid_inc_ship aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_net_paid_inc_ship_tax aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_net_paid_inc_tax aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_net_profit aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_order_number aggregator doubleSum is nullable nullvalue "-93"
dimension cs_promo_sk is nullable nullvalue "-93"
dimension cs_quantity is nullable nullvalue "-93"
metric cs_sales_price aggregator doubleSum is nullable nullvalue "-99999.91"
metric cs_ship_addr_sk aggregator longSum
metric cs_ship_cdemo_sk aggregator longSum
metric cs_ship_customer_sk aggregator longSum
metric cs_ship_date_sk aggregator longSum is nullable nullvalue "-1"
metric cs_ship_hdemo_sk aggregator longSum
dimension cs_ship_mode_sk is nullable nullvalue "-93"
metric cs_sold_date_sk aggregator longSum
metric cs_sold_time_sk aggregator longSum
dimension cs_warehouse_sk is nullable nullvalue "-93"
metric cs_wholesale_cost aggregator doubleSum is nullable nullvalue "-99999.91"
dimension d_day_name is nullable nullvalue "-93"
dimension d_dom is nullable nullvalue "-93"
dimension d_dow is nullable nullvalue "-93"
dimension d_month_seq is nullable nullvalue "-93"
dimension d_moy is nullable nullvalue "-93"
dimension d_qoy is nullable nullvalue "-93"
dimension d_quarter_name is nullable nullvalue "-93"
dimension d_week_seq is nullable nullvalue "-93"
dimension d_year is nullable nullvalue "-93"
timestamp dimension d_date spark timestampformat "yyyy-MM-dd" is nullable nullvalue "1990-01-01"
dimension hd_buy_potential is nullable nullvalue "-93"
dimension hd_dep_count is nullable nullvalue "-93"
dimension hd_income_band_sk is nullable nullvalue "-93"
dimension hd_vehicle_count is nullable nullvalue "-93"
dimension i_brand is nullable nullvalue "-93"
dimension i_brand_id is nullable nullvalue "-93"
dimension i_category is nullable nullvalue "-93"
dimension i_category_id is nullable nullvalue "-93"
dimension i_class is nullable nullvalue "-93"
dimension i_class_id is nullable nullvalue "-93"
dimension i_color is nullable nullvalue "-93"
metric i_current_price aggregator doubleSum is nullable nullvalue "-99999.91"
dimension i_item_id is nullable nullvalue "-93"
dimension i_manager_id is nullable nullvalue "-93"
dimension i_manufact is nullable nullvalue "-93"
dimension i_manufact_id is nullable nullvalue "-93"
dimension i_size is nullable nullvalue "-93"
dimension i_units is nullable nullvalue "-93"
dimension ib_lower_bound is nullable nullvalue "-93"
dimension ib_upper_bound is nullable nullvalue "-93"
dimension p_channel_dmail is nullable nullvalue "-93"
dimension p_channel_email is nullable nullvalue "-93"
dimension p_channel_event is nullable nullvalue "-93"
dimension p_channel_tv is nullable nullvalue "-93"
dimension sm_carrier is nullable nullvalue "-93"
dimension sm_type is nullable nullvalue "-93"
dimension t_hour is nullable nullvalue "-93"
dimension t_meal_time is nullable nullvalue "-93"
dimension t_minute is nullable nullvalue "-93"
dimension w_city is nullable nullvalue "-93"
dimension w_country is nullable nullvalue "-93"
dimension w_state is nullable nullvalue "-93"
dimension w_warehouse_name is nullable nullvalue "-93"
dimension w_warehouse_sq_ft is nullable nullvalue "-93"
OPTIONS(path "${indexfolder}/catalog_sales_snap",
rowflushboundary "${rowflushboundary}",
nonaggregatequeryhandling "push_filters", 
preferredsegmentsize "${preferredsegmentsize}",
indexSizeReduction "${indexSizeReduction}",
defaultnullvalue "-93"
)
