--
-- History
--    hb  08/21   copied from existing design
--    hb  08/21   change large keys to metric: addr_sk, cdemo_sk,
--                   cust_sk, hdemo_sk, item_sk, date_sk, time_sk
--    hb  08/21   remove avgsizeperpartition option
--    hb  08/21   reduce columns from item

create olap index inventory_snap on inventory
dimension d_day_name is nullable nullvalue "-93"
dimension d_dom is nullable nullvalue "-93"
dimension d_dow is nullable nullvalue "-93"
dimension d_month_seq is nullable nullvalue "-93"
dimension d_moy is nullable nullvalue "-93"
dimension d_qoy is nullable nullvalue "-93"
dimension d_week_seq is nullable nullvalue "-93"
dimension d_year is nullable nullvalue "-93"
timestamp dimension d_date spark timestampformat "yyyy-MM-dd" is nullable nullvalue "1990-01-01"
dimension i_brand is nullable nullvalue "-93"
dimension i_category is nullable nullvalue "-93"
metric i_current_price aggregator doubleSum is nullable nullvalue "-99999.91"
dimension i_manufact is nullable nullvalue "-93"
metric i_wholesale_cost aggregator doubleSum is nullable nullvalue "-99999.91"
metric inv_date_sk aggregator longSum
metric inv_item_sk aggregator longSum
metric inv_quantity_on_hand aggregator longSum is nullable nullvalue "-93"
dimension inv_warehouse_sk is nullable nullvalue "-93"
dimension w_city is nullable nullvalue "-93"
dimension w_country is nullable nullvalue "-93"
dimension w_warehouse_name is nullable nullvalue "-93"
dimension w_warehouse_sq_ft is nullable nullvalue "-93"
OPTIONS(path "${indexfolder}/inventory_snap",
rowflushboundary "${rowflushboundary}",
nonaggregatequeryhandling "push_filters", 
preferredsegmentsize "${preferredsegmentsize}",
indexSizeReduction "${indexSizeReduction}",
defaultnullvalue "-93"
)
