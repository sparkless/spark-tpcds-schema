--
-- History
--    hb  08/21   copied from existing design
--    hb  08/21   change large keys to metric: addr_sk, cdemo_sk,
--                   cust_sk, hdemo_sk, item_sk, date_sk, time_sk
--    hb  08/21   remove avgsizeperpartition option

create olap index store_returns_snap on store_returns
dimension c_birth_month is nullable nullvalue "-93"
metric c_birth_year aggregator doubleSum is nullable nullvalue "-93"
metric c_current_addr_sk aggregator longSum
metric c_current_cdemo_sk aggregator longSum
metric c_current_hdemo_sk aggregator longSum
dimension c_first_name is nullable nullvalue "-93"
metric c_first_sales_date_sk aggregator longSum is nullable nullvalue "-1"
metric c_first_shipto_date_sk aggregator longSum is nullable nullvalue "-1"
dimension c_last_name is nullable nullvalue "-93"
dimension c_preferred_cust_flag is nullable nullvalue "-93"
dimension ca_city is nullable nullvalue "-93"
dimension ca_country is nullable nullvalue "-93"
dimension ca_county is nullable nullvalue "-93"
dimension ca_gmt_offset is nullable nullvalue "-999.91"
dimension ca_state is nullable nullvalue "-93"
dimension ca_zip is nullable nullvalue "-93"
dimension cd_credit_rating is nullable nullvalue "-93"
dimension cd_dep_college_count is nullable nullvalue "-93"
dimension cd_dep_count is nullable nullvalue "-93"
dimension cd_dep_employed_count is nullable nullvalue "-93"
dimension cd_education_status is nullable nullvalue "-93"
dimension cd_gender is nullable nullvalue "-93"
dimension cd_marital_status is nullable nullvalue "-93"
dimension cd_purchase_estimate is nullable nullvalue "-93"
dimension d_day_name is nullable nullvalue "-93"
dimension d_dom is nullable nullvalue "-93"
dimension d_dow is nullable nullvalue "-93"
dimension d_month_seq is nullable nullvalue "-93"
dimension d_moy is nullable nullvalue "-93"
dimension d_qoy is nullable nullvalue "-93"
dimension d_quarter_name is nullable nullvalue "-93"
dimension d_week_seq is nullable nullvalue "-93"
dimension d_year is nullable nullvalue "-93"
timestamp dimension d_date spark timestampformat "yyyy-MM-dd" is nullable nullvalue "1990-01-01"
dimension hd_buy_potential is nullable nullvalue "-93"
dimension hd_dep_count is nullable nullvalue "-93"
dimension hd_income_band_sk is nullable nullvalue "-93"
dimension hd_vehicle_count is nullable nullvalue "-93"
dimension i_brand is nullable nullvalue "-93"
dimension i_brand_id is nullable nullvalue "-93"
dimension i_category is nullable nullvalue "-93"
dimension i_category_id is nullable nullvalue "-93"
dimension i_class is nullable nullvalue "-93"
dimension i_class_id is nullable nullvalue "-93"
dimension i_color is nullable nullvalue "-93"
metric i_current_price aggregator doubleSum is nullable nullvalue "-99999.91"
dimension i_item_id is nullable nullvalue "-93"
dimension i_manager_id is nullable nullvalue "-93"
dimension i_manufact is nullable nullvalue "-93"
dimension i_manufact_id is nullable nullvalue "-93"
dimension i_size is nullable nullvalue "-93"
dimension i_units is nullable nullvalue "-93"
dimension ib_lower_bound is nullable nullvalue "-93"
dimension ib_upper_bound is nullable nullvalue "-93"
dimension r_reason_desc is nullable nullvalue "-93"
dimension s_city is nullable nullvalue "-93"
dimension s_company_id is nullable nullvalue "-93"
dimension s_company_name is nullable nullvalue "-93"
dimension s_county is nullable nullvalue "-93"
dimension s_gmt_offset is nullable nullvalue "-999.91"
dimension s_market_id is nullable nullvalue "-93"
dimension s_number_employees is nullable nullvalue "-93"
dimension s_state is nullable nullvalue "-93"
dimension s_store_id is nullable nullvalue "-93"
dimension s_store_name is nullable nullvalue "-93"
dimension s_street_name is nullable nullvalue "-93"
dimension s_street_number is nullable nullvalue "-93"
dimension s_street_type is nullable nullvalue "-93"
dimension s_suite_number is nullable nullvalue "-93"
dimension s_zip is nullable nullvalue "-93"
metric sr_addr_sk aggregator longSum
metric sr_cdemo_sk aggregator longSum
metric sr_customer_sk aggregator longSum
metric sr_fee aggregator doubleSum is nullable nullvalue "-99999.91"
metric sr_hdemo_sk aggregator longSum
metric sr_item_sk aggregator longSum
metric sr_net_loss aggregator doubleSum is nullable nullvalue "-99999.91"
dimension sr_reason_sk is nullable nullvalue "-93"
metric sr_refunded_cash aggregator doubleSum is nullable nullvalue "-99999.91"
metric sr_return_amt aggregator doubleSum is nullable nullvalue "-99999.91"
metric sr_return_amt_inc_tax aggregator doubleSum is nullable nullvalue "-99999.91"
dimension sr_return_quantity is nullable nullvalue "-93"
metric sr_return_ship_cost aggregator doubleSum is nullable nullvalue "-99999.91"
metric sr_return_tax aggregator doubleSum is nullable nullvalue "-99999.91"
metric sr_return_time_sk aggregator longSum
metric sr_returned_date_sk aggregator longSum
metric sr_reversed_charge aggregator doubleSum is nullable nullvalue "-99999.91"
metric sr_store_credit aggregator doubleSum is nullable nullvalue "-99999.91"
dimension sr_store_sk is nullable nullvalue "-93"
metric sr_ticket_number aggregator doubleSum is nullable nullvalue "-93"
dimension t_hour is nullable nullvalue "-93"
dimension t_meal_time is nullable nullvalue "-93"
dimension t_minute is nullable nullvalue "-93"
OPTIONS(path "${indexfolder}/store_returns_snap",
rowflushboundary "${rowflushboundary}",
nonaggregatequeryhandling "push_filters", 
preferredsegmentsize "${preferredsegmentsize}",
indexSizeReduction "${indexSizeReduction}",
defaultnullvalue "-93"
)
