--
-- History
--    hb  08/20   copied from existing design
--    hb  08/20   remove index from promo - date join and cust - date join

create if not exists star schema on store_sales with stats as
   many_to_one index join of store_sales with date_dim
     on ss_sold_date_sk = d_date_sk
   many_to_one index join of store_sales with time_dim
     on  ss_sold_time_sk = t_time_sk
   many_to_one index join of store_sales with item
     on ss_item_sk = i_item_sk
   many_to_one index join of store_sales with customer
     on ss_customer_sk = c_customer_sk
   many_to_one index join of store_sales with customer_demographics
     on ss_cdemo_sk = cd_demo_sk
   many_to_one index join of store_sales with household_demographics
     on ss_hdemo_sk = hd_demo_sk
   many_to_one index join of store_sales with customer_address
     on ss_addr_sk = ca_address_sk
   many_to_one index join of store_sales with store
     on ss_store_sk = s_store_sk
   many_to_one index join of store_sales with promotion
     on ss_promo_sk = p_promo_sk
   many_to_one index join of household_demographics with income_band
     on hd_income_band_sk = ib_income_band_sk
   many_to_one join of store with date_dim
     on s_closed_date_sk = d_date_sk
   many_to_one index join of promotion with date_dim
     on p_start_date_sk = d_date_sk
   many_to_one join of promotion with date_dim
     on p_end_date_sk = d_date_sk
   many_to_one join of promotion with item
     on p_item_sk = i_item_sk
   many_to_one index join of customer with date_dim
     on c_first_shipto_date_sk = d_date_sk
   many_to_one join of customer with date_dim
     on c_first_sales_date_sk = d_date_sk
   many_to_one join of customer with customer_address
     on c_current_addr_sk = ca_address_sk
   many_to_one join of customer with household_demographics
     on c_current_hdemo_sk = hd_demo_sk
   table date_dim
     d_date determines d_date_sk
   table time_dim
     t_time determines t_time_sk
   table store
      s_division_id determines s_division_name
      s_company_id determines s_company_name
   table promotion
     p_promo_id determines p_promo_sk
 
