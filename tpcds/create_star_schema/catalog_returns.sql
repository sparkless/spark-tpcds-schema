--
-- History
--    hb  08/20   copied from existing design
--    hb  08/20   remove index join from cal_center - date,
--                cat_page - date_dim

create if not exists star schema on catalog_returns with stats as
   many_to_one index join of catalog_returns with time_dim
     on  cr_returned_time_sk = t_time_sk
   many_to_one index join of catalog_returns with date_dim
     on cr_returned_date_sk = d_date_sk
   many_to_one index join of catalog_returns with item
     on cr_item_sk = i_item_sk
   many_to_one index join of catalog_returns with customer
     on cr_returning_customer_sk = c_customer_sk
   many_to_one join of catalog_returns with customer
     on cr_refunded_customer_sk = c_customer_sk
   many_to_one join of catalog_returns with customer_address
     on cr_refunded_addr_sk = ca_address_sk
   many_to_one index join of catalog_returns with customer_address
     on cr_returning_addr_sk = ca_address_sk
   many_to_one join of catalog_returns with household_demographics
     on cr_refunded_hdemo_sk = hd_demo_sk
   many_to_one index join of catalog_returns with household_demographics
     on cr_returning_hdemo_sk = hd_demo_sk
   many_to_one index join of catalog_returns with customer_demographics
     on cr_returning_cdemo_sk = cd_demo_sk  
   many_to_one join of catalog_returns with customer_demographics
     on cr_refunded_cdemo_sk = cd_demo_sk
   many_to_one index join of catalog_returns with call_center
     on cr_call_center_sk = cc_call_center_sk
   many_to_one index join of catalog_returns with catalog_page
     on cr_catalog_page_sk = cp_catalog_page_sk
   many_to_one index join of catalog_returns with warehouse
     on cr_warehouse_sk = w_warehouse_sk
   many_to_one index join of catalog_returns with ship_mode
     on cr_ship_mode_sk = sm_ship_mode_sk
   many_to_one index join of catalog_returns with reason
     on cr_reason_sk = r_reason_sk
   many_to_one index join of call_center with date_dim
     on cc_closed_date_sk = d_date_sk
   many_to_one join of call_center with date_dim
     on cc_open_date_sk = d_date_sk
   many_to_one index join of catalog_page with date_dim
     on cp_start_date_sk = d_date_sk
   many_to_one join of catalog_page with date_dim
     on cp_end_date_sk = d_date_sk
   many_to_one join of customer with date_dim
     on c_customer_sk = d_date_sk
   many_to_one index join of customer with household_demographics
     on c_current_hdemo_sk = hd_demo_sk
   many_to_one index join of customer with customer_demographics
     on c_current_cdemo_sk = cd_demo_sk
   many_to_one index join of customer with customer_address
     on c_current_addr_sk = ca_address_sk
   many_to_one index join of household_demographics with income_band
     on hd_income_band_sk = ib_income_band_sk
   table date_dim
     d_date determines d_date_sk
   table time_dim
     t_time determines t_time_sk

