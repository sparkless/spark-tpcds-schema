--
-- History
--    hb  08/20   copied from existing design

create if not exists star schema on inventory with stats as
   many_to_one join of inventory with date_dim
     on inv_date_sk = d_date_sk
  many_to_one join of inventory with item
     on inv_item_sk = i_item_sk
  many_to_one join of inventory with warehouse
     on inv_warehouse_sk = w_warehouse_sk

