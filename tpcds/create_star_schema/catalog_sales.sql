--
-- History
--    hb  08/20   copied from existing design
--    hb  08/20   remove index join from cal_center - date,
--                cat_page - date_dim, promo - date_dim

create if not exists star schema on catalog_sales with stats as
   many_to_one index join of catalog_sales with time_dim
     on  cs_sold_time_sk = t_time_sk
   many_to_one index join of catalog_sales with date_dim
     on cs_sold_date_sk = d_date_sk
   many_to_one join of catalog_sales with date_dim
     on cs_ship_date_sk = d_date_sk
   many_to_one index join of catalog_sales with item
     on cs_item_sk = i_item_sk
   many_to_one index join of catalog_sales with promotion
     on cs_promo_sk = p_promo_sk
   many_to_one index join of catalog_sales with customer
     on cs_bill_customer_sk = c_customer_sk
   many_to_one join of catalog_sales with customer
     on cs_ship_customer_sk = c_customer_sk
   many_to_one index join of catalog_sales with customer_address
     on cs_bill_addr_sk = ca_address_sk
   many_to_one join of catalog_sales with customer_address
     on cs_ship_addr_sk = ca_address_sk
   many_to_one index join of catalog_sales with household_demographics
     on cs_bill_hdemo_sk = hd_demo_sk
   many_to_one join of catalog_sales with household_demographics
     on cs_ship_hdemo_sk = hd_demo_sk
   many_to_one index join of catalog_sales with customer_demographics
     on cs_bill_cdemo_sk = cd_demo_sk
   many_to_one join of catalog_sales with customer_demographics
     on cs_ship_cdemo_sk = cd_demo_sk
   many_to_one index join of catalog_sales with call_center
     on cs_call_center_sk = cc_call_center_sk
   many_to_one index join of catalog_sales with catalog_page
     on cs_catalog_page_sk = cp_catalog_page_sk
   many_to_one index join of catalog_sales with warehouse
     on cs_warehouse_sk = w_warehouse_sk
   many_to_one index join of catalog_sales with ship_mode
     on cs_ship_mode_sk = sm_ship_mode_sk
   many_to_one index join of call_center with date_dim
     on cc_closed_date_sk = d_date_sk
   many_to_one join of call_center with date_dim
     on cc_open_date_sk = d_date_sk
   many_to_one index join of catalog_page with date_dim
     on cp_start_date_sk = d_date_sk
   many_to_one join of catalog_page with date_dim
     on cp_end_date_sk = d_date_sk
   many_to_one index join of promotion with date_dim
     on p_start_date_sk = d_date_sk
   many_to_one join of promotion with date_dim
     on p_end_date_sk = d_date_sk
   many_to_one join of promotion with item
     on p_item_sk = i_item_sk
   many_to_one join of customer with date_dim
     on c_customer_sk = d_date_sk
   many_to_one join of customer with customer_address
     on c_current_addr_sk = ca_address_sk
   many_to_one join of customer with household_demographics
     on c_current_hdemo_sk = hd_demo_sk
   many_to_one join of customer with customer_demographics
     on c_current_cdemo_sk = cd_demo_sk
   many_to_one index join of household_demographics with income_band
     on hd_income_band_sk = ib_income_band_sk
   table date_dim
     d_date determines d_date_sk
   table time_dim
     t_time determines t_time_sk
   table promotion
     p_promo_id determines p_promo_sk
