--
-- History
--    hb  08/20   copied from existing design
--    hb  08/20   remove index join from web_site - date,
--                web_page - date_dim, promo - date_sim

create if not exists star schema on web_sales with stats as
   many_to_one index join of web_sales with time_dim
     on  ws_sold_time_sk = t_time_sk
   many_to_one index join of web_sales with date_dim
     on ws_sold_date_sk = d_date_sk
   many_to_one join of web_sales with date_dim
     on ws_ship_date_sk = d_date_sk
   many_to_one join of web_sales with item
     on ws_item_sk = i_item_sk
   many_to_one index join of web_sales with customer
     on ws_bill_customer_sk = c_customer_sk
   many_to_one join of web_sales with customer
     on ws_ship_customer_sk = c_customer_sk
   many_to_one index join of web_sales with customer_demographics
     on ws_bill_cdemo_sk = cd_demo_sk
   many_to_one join of web_sales with customer_demographics
     on ws_ship_cdemo_sk = cd_demo_sk
   many_to_one index join of web_sales with customer_address
     on ws_bill_addr_sk = ca_address_sk
   many_to_one join of web_sales with customer_address
     on ws_ship_addr_sk = ca_address_sk
   many_to_one index join of web_sales with household_demographics
     on ws_ship_hdemo_sk = hd_demo_sk
   many_to_one join of web_sales with household_demographics
     on ws_bill_hdemo_sk = hd_demo_sk
   many_to_one index join of web_sales with web_site
     on ws_web_site_sk = web_site_sk
   many_to_one index join of web_sales with warehouse
     on ws_warehouse_sk = w_warehouse_sk
   many_to_one index join of web_sales with ship_mode
     on ws_ship_mode_sk = sm_ship_mode_sk
   many_to_one index join of web_sales with web_page
     on ws_web_page_sk = wp_web_page_sk
   many_to_one index join of web_sales with promotion
     on ws_promo_sk = p_promo_sk
   many_to_one index join of web_site with date_dim
     on web_open_date_sk = d_date_sk
   many_to_one  join of web_site with date_dim
     on web_close_date_sk = d_date_sk
   many_to_one index join of web_page with date_dim
     on wp_creation_date_sk = d_date_sk
   many_to_one join of web_page with date_dim
     on wp_access_date_sk = d_date_sk
   many_to_one join of web_page with customer
     on wp_customer_sk = c_customer_sk
   many_to_one index join of promotion with date_dim
     on p_start_date_sk = d_date_sk
   many_to_one join of promotion with date_dim
     on p_end_date_sk = d_date_sk
   many_to_one join of promotion with item
     on p_item_sk = i_item_sk
   many_to_one join of customer with date_dim
     on c_customer_sk = d_date_sk
   many_to_one join of customer with customer_address
     on c_current_addr_sk = ca_address_sk
   many_to_one join of customer with household_demographics
     on c_current_hdemo_sk = hd_demo_sk
   many_to_one join of customer with customer_demographics
     on c_current_cdemo_sk = cd_demo_sk
   many_to_one index join of household_demographics with income_band
     on hd_income_band_sk = ib_income_band_sk
   table date_dim
     d_date determines d_date_sk
   table time_dim
     t_time determines t_time_sk
   table promotion
     p_promo_id determines p_promo_sk
