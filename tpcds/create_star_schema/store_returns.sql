--
-- History
--    hb  08/20   copied from existing design

create if not exists star schema on store_returns with stats as
   many_to_one index join of store_returns with date_dim
     on sr_returned_date_sk = d_date_sk
   many_to_one index join of store_returns with time_dim
     on  sr_return_time_sk = t_time_sk
   many_to_one index join of store_returns with item
     on sr_item_sk = i_item_sk
   many_to_one index join of store_returns with customer
     on sr_customer_sk = c_customer_sk
   many_to_one index join of store_returns with customer_address
     on sr_addr_sk = ca_address_sk
   many_to_one index join of store_returns with household_demographics
     on sr_hdemo_sk = hd_demo_sk
   many_to_one index join of store_returns with customer_demographics
     on sr_cdemo_sk = cd_demo_sk
   many_to_one index join of store_returns with store
     on sr_store_sk = s_store_sk
   many_to_one index join of store_returns with reason
     on sr_reason_sk = r_reason_sk
   many_to_one join of store with date_dim
     on s_closed_date_sk = d_date_sk
   many_to_one join of customer with date_dim
     on c_first_shipto_date_sk = d_date_sk
   many_to_one join of customer with customer_address
     on c_current_addr_sk = ca_address_sk
   many_to_one join of customer with household_demographics
     on c_current_hdemo_sk = hd_demo_sk
   many_to_one join of customer with customer_demographics
     on c_current_cdemo_sk = cd_demo_sk
   many_to_one index join of household_demographics with income_band
     on hd_income_band_sk = ib_income_band_sk
   table date_dim
     d_date determines d_date_sk
   table time_dim
     t_time determines t_time_sk
   table store
      s_division_id determines s_division_name
      s_company_id determines s_company_name
 
