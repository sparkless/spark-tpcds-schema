--
-- History
--    hb  08/20   copied from existing design
--    hb  08/20   remove index join from web_page - date_dim

create if not exists star schema on web_returns with stats as
   many_to_one index join of web_returns with time_dim
     on  wr_returned_time_sk = t_time_sk
   many_to_one index join of web_returns with date_dim
     on wr_returned_date_sk = d_date_sk
   many_to_one index join of web_returns with item
     on wr_item_sk = i_item_sk
   many_to_one index join of web_returns with customer
     on wr_returning_customer_sk = c_customer_sk
   many_to_one join of web_returns with customer
     on wr_refunded_customer_sk = c_customer_sk
   many_to_one index join of web_returns with customer_demographics
     on wr_returning_cdemo_sk = cd_demo_sk
   many_to_one join of web_returns with customer_demographics
     on wr_refunded_cdemo_sk = cd_demo_sk
   many_to_one index join of web_returns with household_demographics
     on wr_returning_hdemo_sk = hd_demo_sk
   many_to_one join of web_returns with household_demographics
     on wr_refunded_hdemo_sk = hd_demo_sk
   many_to_one index join of web_returns with customer_address
     on wr_returning_addr_sk = ca_address_sk
   many_to_one join of web_returns with customer_address
     on wr_refunded_addr_sk = ca_address_sk  
   many_to_one index join of web_returns with web_page
     on wr_web_page_sk = wp_web_page_sk
   many_to_one index join of web_returns with reason
     on wr_reason_sk = r_reason_sk
   many_to_one join of web_page with date_dim
     on wp_creation_date_sk = d_date_sk
   many_to_one index join of web_page with date_dim
     on wp_access_date_sk = d_date_sk
   many_to_one join of web_page with customer
     on wp_customer_sk = c_customer_sk
   many_to_one join of customer with date_dim
     on c_customer_sk = d_date_sk
   many_to_one join of customer with customer_address
     on c_current_addr_sk = ca_address_sk
   many_to_one join of customer with household_demographics
     on c_current_hdemo_sk = hd_demo_sk
   many_to_one join of customer with customer_demographics
     on c_current_cdemo_sk = cd_demo_sk
   many_to_one index join of household_demographics with income_band
     on hd_income_band_sk = ib_income_band_sk
   table date_dim
     d_date determines d_date_sk
   table time_dim
     t_time determines t_time_sk

